//import libraria principala polyglot din graalvm
import org.graalvm.polyglot.*;

import java.util.ArrayList;

//clasa principala - aplicatie JAVA
class Polyglot {
    //metoda privata pentru conversie low-case -> up-case folosind functia toupper() din R
    private static String RToUpper(String token){
        //construim un context care ne permite sa folosim elemente din R
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        //folosim o variabila generica care va captura rezultatul excutiei funcitiei R, toupper(String)
        //pentru aexecuta instructiunea I din limbajul X, folosim functia graalvm polyglot.eval("X", "I");
        Value result = polyglot.eval("R", "toupper(\""+token+"\");");
        //utilizam metoda asString() din variabila incarcata cu output-ul executiei pentru a mapa valoarea generica la un String
        String resultString = result.asString();
        // inchidem contextul Polyglot
        polyglot.close();

        return resultString;
    }

    //metoda privata pentru evaluarea unei sume de control simple a literelor unui text ASCII, folosind PYTHON
    private static int SumCRC(String token){
        //construim un context care ne permite sa folosim elemente din PYTHON
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        //folosim o variabila generica care va captura rezultatul excutiei functiei PYTHON, sum()
        //avem voie sa inlocuim anumite elemente din scriptul pe care il construim spre evaluare, aici token provine din JAVA, dar va fi interpretat de PYTHON
        int Lungime = token.length();
        int result = 0;

        ///aici imi calculez puterile

        Value ceva = polyglot.eval("python", "[9, 3, 1]");
        Value putere1 = ceva.getArrayElement(0);
        Value putere2 = ceva.getArrayElement(1);
        Value putere3 = ceva.getArrayElement(2);
        Value []puteri = {putere1, putere2, putere3};

        for (int i = 0; i < Lungime; i += 3){
            int resultatPartial = 0;
            if (i + 3 <= token.length()) {
                for (int j = 0; j < puteri.length; j++) {
                    resultatPartial += (token.codePointAt(i + j) * puteri[j].asInt());
                }
            }
            ///cazul in care sirul trimuit (hai cam am niste denumiri uneori) are mai putin de 3 caractere
            else {
                if ((Lungime - i) == 1) {
                    resultatPartial += token.codePointAt(i); ///daca sirul are un caracter se face doar cu puterea 0
                }
                if ((Lungime - i) == 2) {
                    resultatPartial += (token.codePointAt(i) * puteri[1].asInt() + token.codePointAt(i + 1));
                    ///daca sirul are doua caractere facem cu puterea 1 si 0
                }
            }
            result += resultatPartial;
        }

        // inchidem contextul Polyglot
        polyglot.close();

        return result;
    }

    //functia MAIN
    public static void main(String[] args) {
        //construim un context pentru evaluare elemente JS
        Context polyglot = Context.create();
        //construim un array de string-uri, folosind cuvinte din pagina web:  https://chrisseaton.com/truffleruby/tenthings/
        Value array = polyglot.eval("js", "[\"command\",\"we\",\"run\",\"we\",\"MB\"];");
        //pentru fiecare cuvant, convertim la upcase folosind R si calculam suma de control folosind PYTHON

        ///hai sa bagam o lista
        int LungimeList = SumCRC("ZZZZZZZZZZZZZZZZZZZZZ") + 1;
        ArrayList <ArrayList <String> > List = new ArrayList<>(LungimeList);
        for(int i=0; i < LungimeList; i++) {
            List.add(new ArrayList<String>());
        }

        for (int i = 0; i < array.getArraySize();i++){
            String element = array.getArrayElement(i).asString();
            String upper = RToUpper(element);
            int crc = SumCRC(upper);
            List.get(crc).add(upper);
            //System.out.println(upper + " -> " + crc);
        }

        for (int i = 0; i < LungimeList; i++){
            int LungimeListCol = List.get(i).size();
            if (LungimeListCol != 0){
                System.out.print(i + " -> ");
                for (int j = 0; j < LungimeListCol; j++){
                    System.out.print(List.get(i).get(j) + " ");
                }
                System.out.println();
            }
        }

        // inchidem contextul Polyglot
        polyglot.close();
    }
}