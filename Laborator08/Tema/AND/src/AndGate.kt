abstract class AndGate(protected open var bridge : Bridge) {
    abstract fun createGate ()
}