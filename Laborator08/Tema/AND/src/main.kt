fun main (Args : Array <String>){
    AndGateTwo (OutputBridge()).setGate1(true).setGate2(true).createGate()
    AndGateThree (OutputBridge()).setGate1(true).setGate2(false).setGate3(true).createGate()
    AndGateFour (OutputBridge()).setGate1(true).setGate2(false).setGate3(true).setGate4(true).createGate()
    AndGateEight (OutputBridge()).setGate1(true).setGate2(true).setGate3(true).setGate4(true).setGate5(true)
        .setGate6(true).setGate7(true).setGate8(true).createGate()
}