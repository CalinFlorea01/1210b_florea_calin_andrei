class AndGateTwo (override var bridge: Bridge) : AndGate(bridge) {
    private var gate1 : Boolean = false
    private var gate2 : Boolean = false

    ///folosim apply pt a putea seta membrii din clasa in linie
    ///se observa in main
    fun setGate1 (_gate1 : Boolean) = apply {
        gate1 = _gate1
    }

    fun setGate2 (_gate2 : Boolean) = apply{
        gate2 = _gate2
    }
    override fun createGate() {
        bridge.Out ()
        println ("Poarta cu doua intrari cu Output ul " + (gate1 && gate2))
    }
}