class AndGateEight (override var bridge: Bridge) : AndGate(bridge) {
    private var gate1 : Boolean = false
    private var gate2 : Boolean = false
    private var gate3 : Boolean = false
    private var gate4 : Boolean = false
    private var gate5 : Boolean = false
    private var gate6 : Boolean = false
    private var gate7 : Boolean = false
    private var gate8 : Boolean = false

    fun setGate1 (_gate1 : Boolean) = apply{
        gate1 = _gate1
    }

    fun setGate2 (_gate2 : Boolean) = apply{
        gate2 = _gate2
    }

    fun setGate3 (_gate3 : Boolean) = apply{
        gate3 = _gate3
    }

    fun setGate4 (_gate4 : Boolean) = apply{
        gate4 = _gate4
    }

    fun setGate5 (_gate5 : Boolean) = apply{
        gate5 = _gate5
    }

    fun setGate6 (_gate6 : Boolean) = apply{
        gate6 = _gate6
    }

    fun setGate7 (_gate7 : Boolean) = apply{
        gate7 = _gate7
    }

    fun setGate8 (_gate8 : Boolean) = apply{
        gate8 = _gate8
    }
    override fun createGate() {
        bridge.Out ()
        println ("Poarta cu patru intrari cu Output ul " + (gate1 && gate2 && gate3 && gate4 && gate5 && gate6 && gate7 && gate8))
    }
}