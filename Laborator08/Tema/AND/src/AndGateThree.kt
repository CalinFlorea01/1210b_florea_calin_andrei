class AndGateThree (override var bridge: Bridge) : AndGate(bridge) {
    private var gate1 : Boolean = false
    private var gate2 : Boolean = false
    private var gate3 : Boolean = false

    fun setGate1 (_gate1 : Boolean) = apply{
        gate1 = _gate1
    }

    fun setGate2 (_gate2 : Boolean) = apply{
        gate2 = _gate2
    }

    fun setGate3 (_gate3 : Boolean) = apply{
        gate3 = _gate3
    }
    override fun createGate() {
        bridge.Out ()
        println ("Poarta cu trei intrari cu Output ul " + (gate1 && gate2 && gate3))
    }
}