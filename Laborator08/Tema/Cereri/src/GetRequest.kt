import khttp.get
import khttp.responses.Response

class GetRequest(_url: String?, _timeOut: Int, _param : Map<String, String>?) : HTTPGet {
    var timeOut : Int = 0
    lateinit var genericReq : GenericRequest

    init {
        if (_url != null && _param != null){
            genericReq = GenericRequest(_url, _param)
            timeOut = _timeOut
        }
    }

    override fun getResponse() : Response?{
        if (timeOut > 100){
            println ("Jale, prea full, TIMEOUT!!!")
            return null
        }
        println ("A primit raspuns")
        return get(url = genericReq.url, data = genericReq.params)
    }
}