import khttp.post
import khttp.responses.Response

class PostRequest(_url: String?, _params: Map<String, String>) {
    lateinit var genericReq : GenericRequest

    init {
        genericReq = GenericRequest(_url, _params)
    }

    fun postData () : Response?{
        if (genericReq != null)
            return post(url = genericReq.url, data = genericReq.params)
        return null
    }
}