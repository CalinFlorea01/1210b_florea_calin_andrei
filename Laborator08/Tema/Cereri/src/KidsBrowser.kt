class KidsBrowser(_cleanGet: CleanGetRequest?, _postReq: PostRequest?) {
    lateinit var cleanGet : CleanGetRequest
    lateinit var postReq : PostRequest

    init {
        if (_cleanGet != null && _postReq != null){
            cleanGet = _cleanGet
            postReq = _postReq
        }
    }

    fun start (){
        val GET = cleanGet.getResponse()
        val POST = postReq.postData()

        if (GET != null && POST != null){
            println ("site is on")
        }
    }
}