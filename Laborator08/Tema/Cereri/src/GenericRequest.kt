open class GenericRequest() : Cloneable {
    var url = ""
    lateinit var params : Map <String, String>

    constructor(_url : String?, _params : Map <String, String>?) : this() {
        if (_url != null && _params != null){
            url = _url;
            params = _params
        }
    }

    override fun clone(): GenericRequest {
        return this.clone()
    }
}