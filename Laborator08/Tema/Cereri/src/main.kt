fun main (Args : Array<String>){
    val Entries : MutableList <GenericRequest> = ArrayList <GenericRequest>()
    val map : Map <String, String> = mapOf ("key1" to "value1", "key2" to "value2")
    val map1 : Map <String, String> = mapOf ("key1" to "value1", "key3" to "value3")
    val map2 : Map <String, String> = mapOf ("key2" to "value2", "key1213" to "value1213")
    Entries.add (GenericRequest("https://www.facebook.com/", map))
    Entries.add (GenericRequest("https://film.list.co.uk/listing/448346-rambo-last-blood/", map1))
    Entries.add (GenericRequest("https://mail.google.com/", map2))
    Entries.add (GenericRequest("https://khttp.readthedocs.io/en/latest/", map))

    Entries.forEach{
        var getRequest = GetRequest (it.url, 10, it.params)
        var cleanGetRequest = CleanGetRequest (getRequest)
        var postRequest = PostRequest (it.url, it.params)
        var kidsBrowser = KidsBrowser (cleanGetRequest, postRequest)
        kidsBrowser.start()
    }
}