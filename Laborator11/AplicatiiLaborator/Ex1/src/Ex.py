import threading
import multiprocessing
from concurrent.futures import ThreadPoolExecutor
import time
import numpy as np

x = 1000
a = np.random.random (x)
def countdown():
    b = a
    for i in range (x):
        for j in range (x):
            if (b[i] > b[j]):
                aux = b[i]
                b[i] = b[j]
                b[j] = aux

    return b

def isPrim (element):
    for i in range (element / 2):
        if element % i == 0:
            return 0
    return 1

def Filter ():
    for i in range (x):
        if isPrim(a[i]) == 0:
            del (a[i])
    print (a)

def ver_1():
    thread_1 = threading.Thread(target=countdown)
    thread_2 = threading.Thread(target=countdown)
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()
def ver_2():
    countdown()
    countdown()
def ver_3():
    process_1 = multiprocessing.Process(target=countdown)
    process_2 = multiprocessing.Process(target=countdown)
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()
def ver_4():
    with ThreadPoolExecutor(max_workers=2) as executor:
        future = executor.submit(countdown())
        future = executor.submit(countdown())
if __name__ == '__main__':
    start = time.time()
    ver_1()
    end = time.time()
    print("\n Timp executie pseudoparalelism cu GIL")
    print(end - start)
    start = time.time()
    ver_2()
    end = time.time()
    print("\n Timp executie secvential")
    print(end - start)
    start = time.time()
    ver_3()
    end = time.time()
    print("\n Timp executie paralela cu multiprocessing")
    print(end - start)
    start = time.time()
    ver_4()
    end = time.time()
    print("\n Timp executie paralela cu concurrent.futures")
    print(end - start)