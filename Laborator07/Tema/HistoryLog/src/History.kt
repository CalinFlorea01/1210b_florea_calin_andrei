import java.io.File
interface Comparable<in T>
{
    abstract operator fun compareTo(other:T):Int
}

class HistorySysLog(private val Start_date : String, private var Command : String) : Comparable<HistorySysLog>
{
    fun GetStart_date():String
    {
        return Start_date
    }

    fun SetCommand(comanda : String)
    {
        Command = comanda
    }
    fun afisare()
    {
        println("Start date: " + Start_date + "\n" + "Command-Line: " + Command)
    }

    ///practim aici supraincarcam compareTo sa faca comparatie dupa timeStamp
    ///comparam obiectul curent cu obiectul historyElement
    override fun compareTo(historyElement : HistorySysLog): Int
    {

        ///desfacem elementul venit ca parametru
        ///in partsHistoryElementDate avem data, in partsHistoryElementHour avem ora
        var timeFullHistory = historyElement.GetStart_date().split(" ")
        var partsHistoryElementDate = timeFullHistory[0].split("-")
        var partsHistoryElementHour = timeFullHistory[1].split(":")

        var partsThis = Start_date.split(" ")
        var partsThisHour = partsThis[1].split(":")
        var partsThisDate = partsThis[0].split("-")

        ///intai comparam dupa data
        for (i in 1..partsThisDate.size-1)
        {
            if(partsThisDate[i].toInt() > partsHistoryElementDate[i].toInt())
            {
                return 1
            }
            else
            {
                if(partsThisDate[i].toInt() < partsHistoryElementDate[i].toInt())
                {
                    return -1
                }
            }
        }

        ///comparam dupa ora
        for(i in 1..partsThisHour.size-1)
        {
            if(partsThisHour[i].toInt() > partsHistoryElementHour[i].toInt())
            {
                return 1
            }
            else
            {
                if(partsThisHour[i].toInt() < partsHistoryElementHour[i].toInt())
                {
                    return -1
                }
            }
        }
        return 0
    }

}

///citim linie cu linie
///split dupa spatii
///daca primul element este date atunci impartim facem mare string din acel timp
///si punem o comanda la misto
///daca primul element este CommandLine inlocuim comanda aia la misto cu comanda curenta
fun readFile(fileName: String):List<HistorySysLog>
{
    var HistoryLog: List<HistorySysLog> = listOf()
    File(fileName).forEachLine {
        var SplitVar = it.split(" ")
        if(SplitVar[0].equals("Start-Date:"))
        {
            var time = SplitVar[1] + SplitVar[2] + " " + SplitVar[3]
            HistoryLog += (HistorySysLog(time,"Ceva de efect, o schimbam dupa :)"))
        }
        else
        {
            if(SplitVar[0].equals("Commandline:"))
            {
                HistoryLog[HistoryLog.size - 1].SetCommand(SplitVar[1])
            }
        }
    }
    return HistoryLog
}

fun mapOfHistoryLog(syslog : List<HistorySysLog>) : MutableMap<String, HistorySysLog>
{
    var map : MutableMap <String, HistorySysLog> = mutableMapOf()
    syslog.forEach {
        if(!map.containsKey(it.GetStart_date()))
            map.put(it.GetStart_date(), it)
        else
            map.put(it.GetStart_date(), it)
    }

    return map
}

fun inlocuire(cautat:HistorySysLog, elem: HistorySysLog, mapa:MutableMap <String, HistorySysLog>)
{
    mapa.keys.forEach {
        if(it.equals(cautat.GetStart_date()))
        {
            mapa.put(it,elem)
        }
    }
}

fun main()
{
    var Historylog : List<HistorySysLog> = readFile("history.log")
    var map : MutableMap<String,HistorySysLog> = mapOfHistoryLog(Historylog)

    if (Historylog[0].compareTo(Historylog[1]) < 0){
        Historylog[0].afisare()
    }
    else {
        Historylog[1].afisare()
    }

    println("\n\n")

    map.values.forEach { it.afisare() }
}