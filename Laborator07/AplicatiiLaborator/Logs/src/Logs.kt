import java.io.File

class SyslogRecord(private val time:String, private val name:String, private val aplication:String, private val pid:String, private val entry:String)
{
    fun Getaplication():String
    {
        return aplication
    }
    fun GetPid():String
    {
        return pid
    }
    fun afisare()
    {
        if(pid != "0")
            println(time+ " "+name+ " "+aplication+" "+pid+" "+entry )
        else
            println(time+ " "+name+ " "+aplication+" "+entry )
    }
}

///luam fiecare linie din fisierul de loguri
///split la linie si construim obiecte pe baza la elementele rezultate de la split
///obiectele sunt bagate intr-o lista
fun readFile (fileName : String) : List< SyslogRecord > {
    var syslog : List<SyslogRecord> = listOf()

    File(fileName).forEachLine {
        var parts = it.split(" ")
        val splitValue = parts[5].split("[")
        var s = ""
        var time = parts[0] + " " + parts[1] + " " + parts[2]

        for (i in 6..(parts.size-1))
            s = s + parts[i]
        var pid=""

        ///daca nu contine paranteze patrate atunci nu avem pid
        ///se pune la membrul pid valoarea 0 in constructor
        if( !parts[5].contains("["))
            pid = "0"
        else
            pid = splitValue[1].replace("]","").replace(":","")

        syslog += (SyslogRecord (time, parts[4], splitValue[0].replace(":",""), pid, s))
    }
    return syslog
}

///avem un map cu cheile nume de aplicatie
///la fiecare cheie vom stoca o lista de loguri
fun MapareLoguri(syslog : List <SyslogRecord>): MutableMap<String, List<SyslogRecord>>
{
    var mapa : MutableMap <String, List <SyslogRecord> > = mutableMapOf()
    syslog.forEach()
    {
        if(!mapa.containsKey (it.Getaplication ()))
            mapa.put(it.Getaplication(),listOf(it))
        else
            mapa.put(it.Getaplication(), mapa.getValue(it.Getaplication()) + it)
    }
    return mapa
}
fun main()
{
    var syslog : List<SyslogRecord> = readFile("syslog")

    var x : List<SyslogRecord> = listOf()
    var map = MapareLoguri(syslog)

    println(map.keys)
    map.values.forEach {
        x = it.sortedBy { String() }
        x.forEach {
            it.afisare()
        }
    }

    ///nu se iau in considerare elementele cu pidul 0
    println("\n\nHai sa scoatem doar alea cu piduri")
    map.values.forEach{
        x = it.filter { !it.GetPid().equals("0") }
        x.forEach {
            it.afisare()
        }
    }
}