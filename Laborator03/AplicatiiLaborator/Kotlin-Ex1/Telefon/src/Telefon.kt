class Birth(val year: Int, val Month: Int, val Day: Int){
    override fun toString() : String{
        return "($Day.$Month.$year)"
    }
}
class Contact(var Name: String, var Phone: String, val BirthDate: Birth){
    fun Print() {
        println("Name: $Name, Mobile: $Phone, Date: $BirthDate")
    }
}

fun FindName (a: List<Contact>, name: String) : Contact{
    for (persoana in a){
        if (persoana.Name.compareTo(name) == 0){
            return persoana;
        }
    }
    return Contact (" ", " ", Birth(0, 0, 0));
}

fun FindPhone(a : List <Contact>, phone : String) : Contact{
    for (persoana in a){
        if (persoana.Phone.compareTo(phone) == 0)
            return persoana;
    }
    return Contact (" ", " ", Birth(0, 0, 0));
}

fun UpdatePhoneNumber (Obj : Contact, phone : String) : Contact{
    Obj.Phone = phone;
    return Obj;
}

fun main(args : Array<String>){
    val agenda = mutableListOf<Contact>()
    agenda.add(Contact("Mihai", "0744321987", Birth(1900, 11, 25)))
    agenda += Contact("George", "0761332100", Birth(2002, 3, 14))
    agenda += Contact("Liviu" , "0231450211", Birth(1999, 7, 30))
    agenda += Contact("Popescu", "0211342787", Birth(1955, 5, 12))
    for (persoana in agenda){
        persoana.Print()
    }
    println("Agenda dupa eliminare contact [George]:")
    agenda.removeAt(1)
    for (persoana in agenda){
        persoana.Print()
    }
    agenda.remove(Contact("Liviu" , "0231450211", Birth(1999, 7, 30)))
    println("Agenda dupa eliminare contact [Liviu]:")
    agenda.removeAt(1)
    for (persoana in agenda){
        persoana.Print()
    }

    println ("Il gasim pe Mihai:");
    FindName(agenda, "Mihai").Print();

    println ("Hai sa il gasim si dupa numar de telefon:");
    FindPhone(agenda, "0211342787").Print();

    println ("Bagam si un update la telefon:");
    agenda[0] = UpdatePhoneNumber(agenda[0],"0761299784");
    agenda[0].Print();
}