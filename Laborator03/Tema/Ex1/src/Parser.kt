import java.io.File
import org.json.JSONObject
import org.json.XML

class SubObject(var link: String, var description: String, var title: String){
    fun Print (){
        println ("Link : $link, description: $description, title: $title")
    }
}

class Object (var link: String, var description: String, var title: String, var Objects: List<SubObject>){
    fun Print (){
        println ("Atribute obiect mare:")
        println ("Link : $link, description: $description, title: $title")
        println ("Atribute de sub-obiecte");
        for (Object in Objects)
            Object.Print()
    }
}

fun main(args: Array<String>) {
    ///conversie din xml in JSON
    ///ma bate prea tare pe xml, cu json le mai am un pic
    val PRETTY_PRINT_INDENT_FACTOR = 2

    val xmlFile = "Text.xml";

    val xmlStr = File(xmlFile).readText()
    val jsonObj = XML.toJSONObject(xmlStr)

    val jsonFile = "ParserText.json";
    File(jsonFile).writeText(jsonObj.toString(PRETTY_PRINT_INDENT_FACTOR))

    ///hai sa ajungem si noi in obiectul channel zic
    val StringFromFile = File (jsonFile).readText()
    val obj = JSONObject (StringFromFile)
    val rss = obj.getJSONObject("rss")
    val channel = rss.getJSONObject("channel")

    ///scoatem atributele lui channel
    val link = channel.getString("link")
    val description = channel.getString("description")
    val title = channel.getString("title")

    ///scoatem atributele pentru vectorul de obiecte
    val SubObjects = channel.getJSONArray("item")
    val ListSubObj = mutableListOf<SubObject>()

    for (i in 0 until SubObjects.length()) {
        val Sub = JSONObject(SubObjects[i].toString())
        val linktemp = Sub.getString("link")
        val descriptiontemp = Sub.getString("description")
        val titletemp = Sub.getString("title")
        ListSubObj.add (SubObject(linktemp, descriptiontemp, titletemp))
    }

    ///creem obiectul mare si il afisam
    val FinalObject = Object (link, description, title, ListSubObj)
    FinalObject.Print()
}

