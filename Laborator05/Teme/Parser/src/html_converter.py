import os
import sys
from PyQt5.QtWidgets import QWidget, QApplication, QFileDialog
from PyQt5.uic import loadUi
from PyQt5 import QtCore
import textile
import sysv_ipc

def send_message(message_queue, message):
    message_queue.send(message)

def debug_trace(ui=None):
    from pdb import set_trace
    QtCore.pyqtRemoveInputHook()
    set_trace()
    # QtCore.pyqtRestoreInputHook()

class HTMLConverter(QWidget):
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    def __init__(self):
        self.text = ""
        super(HTMLConverter, self).__init__()
        ui_path = os.path.join(self.ROOT_DIR, 'html_converter.ui')
        loadUi(ui_path, self)
        self.browse_btn.clicked.connect(self.browse)
        self.ToHTMLBtn.clicked.connect (self.Parse)
        self.ToCBtn.clicked.connect (self.SendToC)
        self.file_path = None

    def Parse (self):
        if self.text == "":
            self.browse()
        self.html = textile.textile (self.text)
        self.TextHTML.setText (self.html)

    def SendToC (self):
        message_queue = sysv_ipc.MessageQueue(-1)
        send_message(message_queue, self.text)

    def browse(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file, _ = QFileDialog.getOpenFileName(self,
                                              caption='Select file',
                                              directory='',
                                              filter="Text Files (*.txt)",
                                              options=options)
        if file:
            self.file_path = file
            self.path_line_edit.setText(file)
            with open('text.txt','r') as file:
                self.text = file.read ()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = HTMLConverter()
    window.show()
    sys.exit(app.exec_())