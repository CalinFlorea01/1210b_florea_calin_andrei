class Note(var Author: String, var Date: Data, var Hour: String, var Text: String){
    fun SetAuthor (NameAuthor : String){
        Author = NameAuthor
    }

    fun GetAuthor () : String{
        return Author
    }

    fun SetText (TextNew : String){
        Text = TextNew
    }

    fun GetText () : String{
        return Text
    }

    fun SetHour (NewHour : String){
        Hour = NewHour
    }

    fun GetHour () : String{
        return Hour
    }

    fun SetDate (newData : Data){
        Date = newData
    }

    fun GetDate () : Data{
        return Date
    }

    fun Print (){
        print ("Author: $Author, Date: ${Date.toString()}, Hour: $Hour\nTextNote:\n$Text\n")
    }
}