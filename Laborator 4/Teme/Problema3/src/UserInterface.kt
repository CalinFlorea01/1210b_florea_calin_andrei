import java.util.*

class UserInterface (var Notes : NoteManager) {
    var Choose = 0
    fun NewNote (){
        print ("Author: ")
        val Author = readLine()
        print ("Date, 3 variables with spaces between them: ")
        val (DateH, DateM, DateS) = readLine()!!.split (' ')
        print ("Hour: ")
        val Hour  = readLine()
        print ("Text: ")
        val Text = readLine()
        val Date = Data(DateH.toInt(), DateM.toInt(), DateS.toInt())
        if (Author != null && Text != null && Hour != null) {
            Notes.CreateANewNote(Author, Date , Hour, Text)
        }
    }

    fun AddNote () {
        print("Author: ")
        val Author = readLine()
        print("Date, 3 variables with spaces between them: ")
        val (DateH, DateM, DateS) = readLine()!!.split(' ')
        print("Hour: ")
        val Hour = readLine()
        print("Text: ")
        val Text = readLine()
        val Date = Data(DateH.toInt(), DateM.toInt(), DateS.toInt())

        if (Author != null && Text != null && Hour != null) {
            val Notenew = Note (Author, Date , Hour, Text)
            Notes.AddNote(Notenew)
        }
    }

    fun DeleteNote (){
        print("Author: ")
        val Author = readLine()
        print("Date, 3 variables with spaces between them: ")
        val (DateH, DateM, DateS) = readLine()!!.split(' ')
        print("Hour: ")
        val Hour = readLine()
        print("Text: ")
        val Text = readLine()
        val Date = Data(DateH.toInt(), DateM.toInt(), DateS.toInt())

        if (Author != null && Text != null && Hour != null) {
            val Notenew = Note (Author, Date , Hour, Text)
            Notes.DeleteNote(Notenew)
        }
    }

    fun Print (){
        Notes.OutNotesList()
    }

    fun UserInteract (){
        val input = Scanner (System.`in`)
        do{
            print ("\n1 - Add a new note\n2 - Create a new Note\n3 - Delete a note\n4 - Print\n0 - Final interaction\n")
            print ("\nYour Choose: ")
            Choose = input.nextInt()
            when (Choose){
                1 -> AddNote()
                2 -> NewNote()
                3 -> DeleteNote()
                4 -> Print()
            }
        }while (Choose != 0)
    }
}