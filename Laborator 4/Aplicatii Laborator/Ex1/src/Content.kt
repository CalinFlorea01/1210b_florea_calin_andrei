class Content (var author : String, var text : String, var name : String, var publisher : String){
    fun getauthor () : String{
        return author;
    }

    fun setauthor (newAuthor : String){
        author = newAuthor;
    }

    fun gettext () : String{
        return text;
    }

    fun settext (newText : String){
        text = newText;
    }

    fun getname () : String{
        return name;
    }

    fun setname (newName : String){
        name = newName;
    }

    fun getpublisher () : String{
        return publisher;
    }

    fun setpublisher (newpublisher : String){
        publisher = newpublisher;
    }
}
