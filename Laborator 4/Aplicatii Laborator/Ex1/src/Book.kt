class Book (var data : Content){
    fun getName () : String{
        return data.getname();
    }

    fun getAuthor () : String{
        return data.getauthor();
    }

    fun getPublisher () : String{
        return data.getpublisher();
    }

    fun getContent () : String{
        return data.gettext();
    }

    override fun toString () : String{
        return "Autorul este: " +  getAuthor() + ", Publisherul este: " + getPublisher() +
                ", Numele este: " + getName() + ", Contentul este: " + getContent();
    }

    fun hasAuthor (AuthorName : String) : Boolean{
        return AuthorName == getAuthor();
    }

    fun hasPublisher (PublisherName : String) : Boolean{
        return PublisherName == getPublisher();
    }

    fun hasContent (ContentData : String) : Boolean{
        return ContentData == getContent();
    }

    fun hasName (NameData : String) : Boolean{
        return NameData == getName();
    }
}