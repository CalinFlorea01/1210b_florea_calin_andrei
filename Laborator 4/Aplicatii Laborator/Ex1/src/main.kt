fun main(args : Array<String>){
    val content1 = Content ("Liviu", "ceva text", "Ion", "cineva");
    val content2 = Content ("Vasea", "ceva text", "Ion", "cineva");
    val content3 = Content ("Gica", "ceva text", "Ion", "cineva");

    val Carte1 = Book (content1);
    val Carte2 = Book (content2);
    val Carte3 = Book (content3);

    val SetLib : Set <Book> = setOf(Carte1, Carte2);
    val Librarie = Library (SetLib)
    Librarie.addbook(Carte3)
}