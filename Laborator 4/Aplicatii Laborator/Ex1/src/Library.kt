class Library (var books : Set <Book>) {
    fun getbooks () : Set <Book>{
        return books;
    }

    fun addbook (newBook : Book){
        books.plus(newBook);
    }

    fun findAllByAuthor (AuthorToFind : String) : Set <Book>{
        val temp : Set <Book> = setOf();
        for (Iterator in books){
            if (Iterator.hasAuthor(AuthorToFind))
                temp.plus(Iterator);
        }
        return temp;
    }

    fun findAllByName (NameToFind : String) : Set <Book>{
        val temp : Set <Book> = setOf();
        for (Iterator in books){
            if (Iterator.hasName(NameToFind))
                temp.plus(Iterator);
        }
        return temp;
    }

    fun findAllByPublisher (PublisherToFind : String) : Set <Book>{
        val temp : Set <Book> = setOf();
        for (Iterator in books){
            if (Iterator.hasName(PublisherToFind))
                temp.plus(Iterator);
        }
        return temp;
    }
}