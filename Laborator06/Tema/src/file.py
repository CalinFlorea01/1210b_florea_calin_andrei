import os
import xml.etree.ElementTree as ET
import PIL

class GenericFile:
    def get_path(self):
        pass
    def get_freq(self):
        pass

class TextASCII(GenericFile):
    path_absolut : str
    frecvente = []

    def __init__(self, _path, _frecvente):
        self.path_absolut = _path
        self.frecvente = _frecvente

    def get_freq(self):
        return self.frecvente

    def get_path(self):
        return self.path_absolut

    #supraincarcam functie de sistem, cand dam print la obiect afiseaza path_absolut
    def __repr__(self):
        return self.path_absolut

class TextUNICODE(GenericFile):
    path_absolut : str
    frecvente = []

    def __init__(self, _path, _frecvente):
        self.path_absolut = _path
        self.frecvente = _frecvente

    def get_freq(self):
        return self.frecvente

    def get_path(self):
        return self.path_absolut

    #supraincarcam functie de sistem, cand dam print la obiect afiseaza path_absolut
    def __repr__(self):
        return self.path_absolut

class TextBinary(GenericFile):
    path_absolut : str
    frecvente = []

    def __init__(self, _path, _frecvente):
        self.path_absolut = _path
        self.frecvente = _frecvente

    def get_freq(self):
        return self.frecvente

    def get_path(self):
        return self.path_absolut

    #supraincarcam functie de sistem, cand dam print la obiect afiseaza path_absolut
    def __repr__(self):
        return self.path_absolut

class BMP (TextBinary):
    width : int
    height : int
    bpp : int

    def __init__(self,  _path, _frecvente):
        super().__init__(_path, _frecvente)
        image = PIL.Image.open (_path)
        self.width, self.height = image.size

    def show_info (self):
        print ("Width: ", self.width, "Height", self.height)

class XMLFile (TextASCII):
    first_tag : str

    def __init__(self, _path, _frecvente):
        super().__init__(_path, _frecvente)
        tree = ET.parse (self.get_path())
        root = tree.getroot()
        self.first_tag = root.tag

    def get_first_tag (self):
        return self.first_tag

#numaram de cate ori apare fiecare caracter dintr-un fisier pt care avem path-ul
def FormFrecv(path):
    frecventa = []

    for i in range (0, 255):
        frecventa.append (0)

    file = open (path, "r")

    for line in file:
        for i in line:
            if (ord (i) > 256):
                raise Exception ("Vezi ca ai bagat niste caractere peste nivelul codului meu de gandire")
            frecventa[ord (i)] = frecventa[ord (i)] + 1

    return frecventa

#in functie de frecventa cu care apar caracterele in fisier determinam de ce tip este
def WhatAType (frecventa):
    s0 = 0 #frecventa 9, 10, 13, 32...127
    s1 = 0 #frecventa 0...8, 11, 12, 14, 15...31, 128...255
    frecventaMinima = frecventa[0]
    frecventaMaxima = frecventa[0]
    frecventaTotala = 0

    for i in range(0, 255):
        #tratam intai pt ASCII/UTF8
        if i == 9 or i == 10 or i == 13 or i in range (32, 127):
            s0 = s0 + frecventa[i]
        if i in range (0, 8) or i == 11 or i == 12 or i == 14 or i in range (15, 31) or i in range (128, 255):
            s1 = s1 + frecventa[i]
        #tratam pentru binar
        #frecventa minima frecventa maxima si vedem daca este o diferenta foarte mare
        if frecventa[i] > frecventaMaxima:
            frecventaMaxima = frecventa[i]
        if frecventa[i] < frecventaMinima:
            frecventaMinima = frecventa[i]
        frecventaTotala = frecventaTotala + frecventa[i]

    #si acum sa inceapa distractia
    #unicode, vedem daca se afla 0 in cel putin 30% din toata frecventa
    #caracterul 0, pe la 48 in ascii table
    if frecventa[48] >= 0.3 * frecventaTotala:
        return "UNICODE"

    #nu-i unicode, nice. trecem la Ascii/UTF8
    #ce inseamna o diferenta mare de frecventa nu prea stiu. cred ca un 97% vs 3% e ceva
    if frecventaTotala * 0.97 <= s0 and frecventaTotala * 0.03 >= s1:
        return "ASCII"

    #nici ASCII. hmm. hai sa incercam si binar
    #ce ar trebui sa insemne uniform nu stiu, am luat in considerare ca ar fi diferente minore intre frecventa maxima si minima
    #acum ce poate insemna diferenta minora, am zis de maxim 5% din frecventa totala
    if frecventaMaxima - frecventaMinima <= 0.05 * frecventaTotala:
        return "BINAR"

    #ce fisier misto am primit, nu-i de niciun fel
    return "Sante!"

if __name__=='__main__':
    AsciiList = []
    UnicodeList = []
    BinarList = []

    #incepem sa cautam fisiere din directorul de lucru
    path = os.path.dirname(os.path.abspath(__file__))

    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if '.txt' in file or '.xml' in file or '.bmp' in file:
                pathCurentFile = os.path.join(r, file)
                frecventa = FormFrecv(pathCurentFile)
                type = WhatAType(frecventa)
                if type == "ASCII":
                    AsciiList.append(TextASCII(pathCurentFile, frecventa))
                elif type == "UNICODE":
                    UnicodeList.append(TextUNICODE(pathCurentFile, frecventa))
                elif type == "BINAR":
                    BinarList.append(TextBinary(pathCurentFile, frecventa))

    print("Lista ascii:")
    print(AsciiList)
    print ("Lista unicode:")
    print(UnicodeList)
    print ("Lista binar:")
    print(BinarList)