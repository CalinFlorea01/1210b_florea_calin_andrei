import os
import subprocess
from abc import ABC


class Handler(ABC):
    def handle(self, request):
        pass


class baseHandler(Handler):
    next: Handler

    def setNext(self, _handler: Handler):
        self.next = _handler

    def handle(self, request):
        if self.next:
            return self.next.handle(request)
        return None


class JavaHandler(baseHandler):
    def handle(self, request):
        if request == "System." or request == "main(String[]" or request == "void":
            return "Java"
        else:
            return super().handle(request)


class KotlinHandler(baseHandler):
    def handle(self, request):
        if request == "var" or request == "val" or request == "fun":
            return "Kotlin"
        else:
            return super().handle(request)


class PythonHandler(baseHandler):
    def handle(self, request):
        if request == "def" or request == "__name__" or request == "__main__":
            return "Python"
        else:
            return super().handle(request)

class BashHandler (baseHandler):
    def handle(self, request):
        if request == "#!/bin/bash":
            return "Bash"
        else:
            return super ().handle(request)


#luate de pe stack astea :)
def compile_java(java_file):
    subprocess.check_call(['javac', java_file])

def execute_java(java_file):
    cmd = ['java', java_file]
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    input = subprocess.Popen(cmd, stdin=subprocess.PIPE)
    print(proc.stdout.read())

def executeBashFile (bash_file):
    return_code = subprocess.call("./" + bash_file)
    print (return_code)

def execute_python (python_file):
    exec (open(python_file).read())

def executeFile (typeOfFile, file):
    if typeOfFile == "Bash":
        executeBashFile(file)
    if typeOfFile == "Java":
        compile_java(file)
        execute_java(file)
    if typeOfFile == "Python":
        execute_python(file)

def Client (listOfFiles):
    java = JavaHandler ()
    kotlin = KotlinHandler ()
    bash = BashHandler ()
    python = PythonHandler ()

    java.setNext(kotlin)
    kotlin.setNext(bash)
    bash.setNext(python)

    for filePath in listOfFiles:
        file = open (filePath, "r")
        executeContor = 0
        for line in file:
            lineElements = line.split(" ")
            for lineElement in lineElements:
                handleResult = baseHandler.handle(lineElement)
                if handleResult:
                    executeFile(handleResult, filePath)
                    executeContor = 1
                    break
            if executeContor == 1:
                break
        if executeContor == 0:
            print ("Tip necunoscut")

if __name__ == "__main__":
    list = []
    #incepem sa cautam fisiere din directorul de lucru
    path = os.path.dirname(os.path.abspath(__file__))

    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            pathCurentFile = os.path.join(r, file)
            list.append (pathCurentFile)

    Client (list)
    print (list)