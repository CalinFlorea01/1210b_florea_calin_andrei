from abc import ABC
from datetime import datetime

import requests

class dataSite (ABC):
    def Request (self):
        pass

def timetoSeconds():
    return (datetime.now().hour * 3600 + datetime.now().minute * 60 + datetime.now().second)

class newDataSite (dataSite):
    urlRequest = ""
    def __init__(self, urlPath):
        self.urlRequest = requests.get (urlPath)

    def readFromFile (self, fileName):
        file = open (fileName, "r")
        line = file.readline()
        fileStuff = ""
        while line:
            fileStuff = fileStuff + line
            line = file.readline()
        file.close ()
        return fileStuff

    def Request(self):
        fileStuff = self.readFromFile("Cache.txt")

        file = open ("Cache.txt", "w")

        for line in fileStuff:
            file.write(line)

        file.write(self.urlRequest.url)
        file.write ("\n")
        file.write(str (self.urlRequest.elapsed.total_seconds()))
        file.write("\n")
        file.write(str (timetoSeconds()))
        file.write("\n")
        file.write(str (self.urlRequest.status_code) + "\n")
        file.close()

class cacheDataSite (dataSite):
    newdataSite : newDataSite
    def __init__(self, _newddataSite : newDataSite):
        self.newdataSite = _newddataSite

    def Request(self):
        file = open ("Cache.txt", "r")
        line = file.readline()
        while line:
            if line == self.newdataSite.urlRequest.url:
                file.readline()
                line = file.readline()
                if (int (line) >= timetoSeconds() - 3600):
                    print (file.readline())
                else:
                    newDataSite (self.newdataSite.urlRequest.url).Request()
                return
            line = file.readline()
        newDataSite (self.newdataSite.urlRequest.url).Request()

if __name__ == "__main__":
    newdatasite = newDataSite ("https://refactoring.guru/design-patterns/proxy")
    newdatasite.Request()
    cacheDataSite (newDataSite("https://refactoring.guru/design-patterns/what-is-pattern")).Request()