import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Stack;
import javax.swing.*;

public class Calculator extends JFrame {
    JButton digits[] = {
            new JButton(" 0 "),
            new JButton(" 1 "),
            new JButton(" 2 "),
            new JButton(" 3 "),
            new JButton(" 4 "),
            new JButton(" 5 "),
            new JButton(" 6 "),
            new JButton(" 7 "),
            new JButton(" 8 "),
            new JButton(" 9 ")
    };

    JButton operators[] = {
            new JButton(" + "),
            new JButton(" - "),
            new JButton(" * "),
            new JButton(" / "),
            new JButton(" = "),
            new JButton(" C "),
            new JButton(" ( "),
            new JButton(" ) ")
    };

    String oper_values[] = {"+", "-", "*", "/", "=", "", "(", ")"};

    String value;
    char operator;

    JTextArea area = new JTextArea(3, 5);

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.setSize(430, 400);
        calculator.setTitle(" Java-Calc, PP Lab1 ");
        calculator.setResizable(false);
        calculator.setVisible(true);
        calculator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public boolean operator (Character s){
        return s == '*' || s == '/';
    }

    public StringBuilder PostFixata (String a){
        Stack<Character> st = new Stack<>();
        StringBuilder Out = new StringBuilder();
        char caracter;

        for (int i = 0; i < a.length(); i++){
            caracter = a.charAt(i);
            if (caracter >= '0' && caracter <= '9'){
                Out.append(caracter);
            }
            else
            {
                if (caracter == '(') {
                    st.push(caracter);
                }
                else {
                    if (caracter == ')') {
                        while (st.peek() != '(') {
                            Out.append(st.pop());
                        }
                    }
                    else
                        if (!st.empty()){
                            while (operator (st.peek()) && st.peek() != '(' && !st.empty()){
                                Out.append(st.pop());
                            }
                        }
                    st.push(caracter);
                }
            }
        }

        while (!st.empty()){
            if (st.peek() != '(' && st.peek() != ')')
                Out.append(st.peek());
            st.pop();
        }

        return Out;
    }

    public double EvaluareExpresie (StringBuilder a){
        Stack <Double> st = new Stack<> ();
        double Out = 0;
        char character;
        double operator1, operator2;
        for (int i = 0; i < a.length(); i++){
            character = a.charAt(i);

            if (character >= '0' && character <= '9'){
                st.push(Double.parseDouble(String.valueOf(character)));
            }
            else {
                operator1 = st.pop();
                operator2 = st.pop();
                if (character == '+'){
                    st.push(operator2 + operator1);
                }
                if (character == '-'){
                    st.push(operator2 - operator1);
                }
                if (character == '*'){
                    st.push(operator2 * operator1);
                }
                if (character == '/'){
                    st.push(operator2 / operator1);
                }
            }
        }
        Out = st.pop();
        return Out;
    }

    public Calculator() {
        add(new JScrollPane(area), BorderLayout.NORTH);
        JPanel buttonpanel = new JPanel();
        buttonpanel.setLayout(new FlowLayout());

        for (int i=0;i<10;i++)
            buttonpanel.add(digits[i]);

        for (int i=0;i<8;i++)
            buttonpanel.add(operators[i]);

        add(buttonpanel, BorderLayout.CENTER);
        area.setForeground(Color.BLACK);
        area.setBackground(Color.WHITE);
        area.setLineWrap(true);
        area.setWrapStyleWord(true);
        area.setEditable(false);

        for (int i=0;i<10;i++) {
            int finalI = i;
            digits[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    area.append(Integer.toString(finalI));
                }
            });
        }

        for (int i=0;i<8;i++){
            int finalI = i;
            operators[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    if (finalI == 5)
                        area.setText("");
                    else
                    if (finalI == 4) {

                        try {
                            area.append(" = " + EvaluareExpresie(PostFixata(area.getText())));
                        } catch (Exception e) {
                            area.setText(" !!!Probleme!!! ");
                        }
                    }
                    else {
                        area.append(oper_values[finalI]);
                        operator = oper_values[finalI].charAt(0);
                    }
                }
            });
        }
    }
}