import itertools

def greaterFive (List):
    return list (filter (lambda x: x >= 5, List))

def Perechi (List):
    list1 = List[0 : len (List) : 2]
    list2 = List[1 : len (List) : 2]

    return list (itertools.zip_longest (list1, list2))

def multiplicarePerechi (List):
    list1 = []
    for element in List:
        list1.append(element[0] * element[1])
    return list1

def suma (List):
    return sum (List)

if __name__ == "__main__":
    List =  [1, 21, 75, 39, 7, 2, 35, 3, 31, 7, 8]
    print (List)
    print (suma(multiplicarePerechi(Perechi(greaterFive(List)))))
