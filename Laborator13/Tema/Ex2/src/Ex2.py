import more_itertools

def read (path):
    words = []
    file = open (path, 'r')
    fileLines = file.readlines ()

    for line in fileLines:
        lineElements = line.split (" ")
        for lineElement in lineElements:
            words.append(lineElement)
    return words

def makeMap (List):
    key_func = lambda wordFirst : wordFirst[0]
    return more_itertools.map_reduce(List, key_func)

if __name__ == "__main__":
    list = read("file.txt")
    print (makeMap(list))