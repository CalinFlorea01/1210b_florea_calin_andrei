import java.io.File

fun makeList (string : String) : List <String>{
    return string.split (" ");
}

fun Caesar (string : String, offset : Int) : String{
    var stringReturn : String = ""

    string.forEach {
        var el = it + offset
        if (el > 'z'){
            el = 'a' + (el - 'z' - 1)
        }
        stringReturn += el
    }
    return stringReturn
}

fun FilterCaesar (list : List <String>, offset : Int) : List <String>{
    var list1 = list.filter { it.length in 4..7 }
    var listReturn : MutableList <String> = mutableListOf()
    list1.forEach {
        listReturn.add (Caesar(it, offset))
    }
    return listReturn
}

fun main (Args : Array <String>){
    File("file.txt").forEachLine { println (FilterCaesar(makeList(it), 5)) }
}