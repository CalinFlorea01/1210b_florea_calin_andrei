fun lessFive (list : List <Int>): List<Int> {
    return list.filter { it > 5}
}

fun group (list : List <Int>) : List < Pair<Int, Int> > {
    var index = 0
    val (even, odd) = list.partition {index++ % 2 == 0}
    return even.zip(odd)
}

fun Multiply (list : List < Pair <Int, Int> >) : List <Int>{
    var listReturn : MutableList <Int>  = mutableListOf()
    list.forEach(){
        listReturn.add (it.first * it.second)
    }
    return listReturn
}

fun Suma (list : List < Int >) : Int{
    var Suma : Int = 0
    list.forEach {
        Suma += it
    }
    return Suma
}
fun main (Args : Array <String>){
    var list = listOf<Int>(1, 21, 75, 39, 7, 35, 31, 7, 8, 2, 3)

    println (Suma (Multiply(group(lessFive(list)))))
}