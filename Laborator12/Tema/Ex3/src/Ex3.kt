import java.io.File
import kotlin.math.sqrt

fun distance (a : Pair <Double, Double>, b : Pair <Double, Double>) : Double {
    return sqrt((b.second - a.second) * (b.second - a.second) + (b.first - a.first) * (b.first - a.first))
}

fun Perimetru (list : List <Pair <Double, Double> >) : Double {
    var list1 = listOf<Pair <Pair<Double, Double>, Pair <Double, Double> >>()
    list1 = list.zipWithNext()

    var perimetru : Double = 0.0

    list1.forEach {
        perimetru += distance(it.first, it.second)
    }
    perimetru += distance(list[0], list.last())
    return perimetru
}

fun main (Args : Array <String>){
    var list : MutableList < Pair <Double, Double> > = mutableListOf()
    var leng = -1

    File("File.txt").forEachLine {
        if (leng == -1){
            leng = it.toInt()
        }
        else{
            val el = it.split(" ")
            val el1 = el[0].toDouble()
            val el2 = el[1].toDouble()
            list.add (Pair (el1,  el2))
        }
    }
    print (Perimetru(list))
}