fun Int.IsPrim () : String{
    for (i in 2..this / 2){
        if (this % i == 0)
            return "Nu e prim"
    }
    return "E prim"
}

fun main (Args : Array<String>){
    val a : Int
    a = 10
    println (a.IsPrim())

    val b : Int
    b = 7
    println(b.IsPrim())
}