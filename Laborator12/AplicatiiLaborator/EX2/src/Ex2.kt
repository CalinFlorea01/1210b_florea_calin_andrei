import java.util.*

fun String.toDate () : Date{
    val componentsDate = this.split (",")
    return Date (componentsDate[0].toInt(), componentsDate[1].toInt(), componentsDate[2].toInt())
}

fun main (Args : Array <String>){
    println ("23,10,2020".toDate())
}